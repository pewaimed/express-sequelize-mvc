var chai   = require('chai')
var assert = chai.assert
var expect = chai.expect
var should = chai.should

var objectStorage = require('../src/object-storage')

describe('ObjectStorage', function () {
  describe('set', function () {
    var testObject = new Object()
    var instance   = new objectStorage()
    
    it('contains a new object after it has been set', function () {
      instance.set('testObject', testObject)
      expect(instance).to.have.property('testObject')
    })
  })
  
  describe('onObjectsAvailable', function () {
      var testObject = new Object()
      var instance   = new objectStorage()
      
      it('notifies observers after all objects are available', function (done) {
        instance.onObjectsAvailable(['object1', 'object2'], function () {
          expect(instance).to.have.property('object1')
          expect(instance).to.have.property('object2')
          done()
        })
        
        instance.set('object1', new Object())
        instance.set('object2', new Object())
      })
  })
})