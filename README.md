# Preferred App Structure
* app.js
* config/
	* express.js
	* routes.js
	* sequelize.js
* controllers/
* models/
* views/

# Configure ESMVC
app.js
```
#!javascript
require('express-sequelize-mvc')({
	config: __dirname + '/config',
	models: __dirname + '/models',
	controllers: __dirname + '/controllers',
	Sequelize: require('Sequelize'),
	express: require('express'),
	verbose: true
}, function (error, boot) {
	var myServerPort = 9000
	boot.app.listen(myServerPort)
});
```

config/express.js
```
#!javascript
module.exports = function (app, express) {
	app.set('views', __dirname + '/../views')
	app.use(express.static(__dirname + '/../assets'));
}
```


config/routes.js
```
#!javascript
module.exports = function (app, controllers, express) {
	app.get('/', controllers.index.index);
}
```

config/sequelize.js
```
#!javascript
module.exports = function (Sequelize) {
	return new Sequelize('root', '');
}
```

# Example Controller
controllers/index.js
```
#!javascript
module.exports.index = function (req, res) {
}
```


# Example Model
```
#!javascript
module.exports = function (sequelize, TYPE, models) {
  var refreshToken = sequelize.define('refresh_tokens', {
	access_token: TYPE.STRING,
	expires: TYPE.DATE
  });

  models.onObjectsAvailable(['user', 'client', 'refreshToken'], function () {
	models.refreshToken.belongsTo(models.user, {
	  foreignKey: 'user_id'
	});

	models.refreshToken.belongsTo(models.client, {
	  foreignKey: 'client_id'
	});
  })

  models.set('refreshToken', refreshToken)
}
```