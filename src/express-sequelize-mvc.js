var fs = require('fs')
var assert = require('assert')
var ObjectStorage = require('./object-storage')

/**
 * Loads files in a given path into an object
 *
 * @param  String   path
 * @return Function callback - Will be called for each file
 */
function loadDir (path, callback) {
  var isValidPath = (path && fs.existsSync(path))
  var isDirectory = isValidPath && fs.statSync(path).isDirectory()

  if (isDirectory) {
    /*
     * For each file in the directory
     */
    fs.readdirSync(path).forEach(function(file) {
      /*
       *  - get the module name (by replacing the file ending
       *  - load the module with require
       *  - apply the callback
       */
      var name   = file.replace('.js', '');
      var module = require([path, name].join('/'))
      callback.apply(callback, [module, name])
    })
  }
}

/**
 * Binds models to request
 * @param  Object models
 * @return Function
 */
function modelBinderMiddleware (models) {
  return function (req, res, next) {
    req.models = models
    next()
  }
}


/**
 * Main module
 *
 * @param Object   config   - Config Object
 * @param Function callback - Will be called once setup is complete
 */
module.exports = function (config, callback) {
  assert(config.config, 'Missing config path')
    
  /**
   * @param String message
   * @return void
   */
  function log (message) {
    config.verbose && console.log.apply(console.log, arguments)
  }
  
  var configPath = config.config
  
  /* Load and configure sequelize */
  var Sequelize = config.Sequelize
  var db        = null
  var models    = null
  
  if (Sequelize) {
    
    /* Create a new sequelize instance */
    log('Loading Database');
    db = require([configPath, 'sequelize.js'].join('/'))(Sequelize)
    assert(db, 'Invalid database. \'sequelize.js\' is required to return a database object.')
    
    /* Load models */
    if (config.models) {
      log('Loading Models');
      models = new ObjectStorage()
      loadDir(config.models, function (model) {
        model(db, config.Sequelize, models)
      })
    }
    
  }
  
  /* load controllers */
  var controllers = null
  if (config.controllers) {
    controllers = {}
    loadDir(config.controllers, function (controller, name) {
      controllers[name] = controller
    })
  }

  // load and bind express
  var express     = config.express
  var app         = null
  if (express) {
    app = express()
        
    /* use middleware to bind models to the request */
    if (models) {
      app.use(modelBinderMiddleware(models))
    }
    
    /* configure express */
    log('configuring express')
    require([configPath, 'express.js'].join('/'))(app, express)
  
    /* load routes */
    log('binding routes')
    require([configPath, 'routes.js'].join('/'))(app, controllers, express)
  }

  /* put everything into an object */
  var boot = {
    app:         app,
    express:     express,
    controllers: controllers,
    models:      models,
    sequelize:   config.sequelize,
    db:          db,
    settings:    config
  }
  
  /* extend app with boot */
  if (app) {
    for (item in boot) {
      if (!(item in app)) app[item] = boot[item]
    }
  }

  log('ready.')
  callback(null, boot)
}
