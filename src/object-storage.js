module.exports = function () {
  var self = this
  
  /**
   * Sets a Object
   *
   * @param string name
   * @param Object  Object
   * @return void
   */
  this.set = function (name, object) {
    self[name] = object
    notifyObservers()
  }
  
  /**
   * Object Define Callbacks
   * @var array
   */
  var observers = []
  
  /**
   * Adds an enventlistener for added objects
   *
   * @param array    requiredObjects
   * @param function callback
   * @return void
   */
  this.onObjectsAvailable = function (requiredObjects, callback) {
    if (requiredObjects && callback) {
      observers.push([requiredObjects, callback])
    }
  }
  
  /**
   * Checks observers if required objects are available
   *
   * @return void
   */
  function notifyObservers () {
    observers.forEach(function(listener, i) {
      var requiredObjects = listener[0]
      var callback = listener[1]
      var conditionsMet = true
      
      // check if required objects are available
      for (var i = 0; (i < requiredObjects.length) && conditionsMet; i++) {
        conditionsMet = (requiredObjects[i] in self);
      }
      
      // remove listener
      if (conditionsMet) {
        observers.splice(i, 1)
        callback()
      }
    })
  }
  
  this.notifyObservers = notifyObservers;
  
}